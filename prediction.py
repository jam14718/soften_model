import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn import datasets
from sklearn import metrics
from sklearn.naive_bayes import GaussianNB
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn import metrics
import json
import requests

data = pd.read_csv(r'SolarPrediction.csv')
# down code is select column form head name table
df = pd.DataFrame(data, columns= ['Generated','Efficiency','Exported','Conditions','Temperature']) 
ax = plt.gca()
# replace data
# if data in table 0 change to NaN
# data - change to NaN
# Not Sure chang to NaN
# Condition -> Showers is 0
#              Fine is 1
#              Partly Cloudy is 2
#              Mostly Cloudy is 3
#              Cloudy is 4
df.replace({
    '-': np.NaN,
    'Not Sure' : '5',
    'Showers' : '0',
    'Fine' : '1',
    'Partly Cloudy' : '2',
    'Mostly Cloudy' : '3',
    'Cloudy' : '4'
},inplace = True)
# -> print data all form after replace
# cleaning data by correcting
# delete data colum Generated then data is NaN 
dataclean_one = df[pd.notnull(df['Generated'])] # remove data Genter
dataclean_one = dataclean_one[pd.notnull(dataclean_one['Temperature'])] # remove data of temperature

# print(dataclean_one)
#print(dataclean_one.isnull().sum()) # -> is show sum of null all row
# Generated        0
# Efficiency       0
# Exported        75
# Peak Power      14
# Peak Time        8
# Conditions       0
# Temperature     71
dataclean_one.Efficiency=pd.to_numeric(dataclean_one.Efficiency)
dataclean_one.Exported=pd.to_numeric(dataclean_one.Exported)
dataclean_one.Conditions=pd.to_numeric(dataclean_one.Conditions)
# find null data of Exported for replace null data 
# print(dataclean_one.loc[(dataclean_one['Exported'].isnull()) & (dataclean_one['Conditions'] == 2)]) # test condition
def findNullData_Con(topic,Condition):
    dataframe = dataclean_one.loc[dataclean_one['Exported'].isnull()]
    if(len(dataframe.index) > 0):
        dataframe = dataframe.loc[dataframe[topic] == Condition]
        mean =  Meandata(topic,Condition)
        # replace data 
        dataframe.replace({
            np.NaN : mean,
        },inplace = True)
        for i in dataframe.index:
            dataclean_one.at[i,'Exported'] = mean
        return dataframe

# -> data before use mean Imputation
# Mean Imputation is find mean data of row
# Mean of Exported
def Meandata(topic,condition):
    dataframe = dataclean_one.loc[dataclean_one[topic] == condition]
    mean = dataframe['Exported'].mean()
    return mean

# handing data
# split data between Temperature
tem = dataclean_one['Temperature'].str.split(" ", n = 1, expand = True) 
temstart = tem[0]
temstop = tem[1].str.split(" ", n = 1, expand = True)
temstop = temstop[1].str.split("C", n = 1, expand = True)
temstop = temstop[0]
# new dataframe
Datacleanup = [dataclean_one,temstart,temstop]
result = pd.concat(Datacleanup, axis=1, sort=False) 
# rename columns 
result.columns = ['Generated','Efficiency','Exported','Conditions','Temperature','temstar','temstop']
# drop columns Temperature
result = result.drop('Temperature',axis=1)
# call function for replace data 
df0 = findNullData_Con('Conditions',0)
df2 = findNullData_Con('Conditions',2)
df1 = findNullData_Con('Conditions',1)
df3 = findNullData_Con('Conditions',3)
df4 = findNullData_Con('Conditions',4)
# change object to int
result.temstar = pd.to_numeric(result.temstar)
result.temstop = pd.to_numeric(result.temstop)
result.Generated = pd.to_numeric(result.Generated)
print(result.info())
# plot graph
result.plot(kind='scatter',x= 'Generated',y='Conditions',ax=ax)
plt.title('Generated vs Exported')
plt.xlabel('Generated')
plt.ylabel('Exported')
# plt.show()
# print(dataclean_one.isnull().sum()) 
# print(temstart)
# print(temstop)
print(result['Generated'].max())
print(result.describe())
# change type 
# export file out to floder
# export_csv = result.to_csv (r'/Users/jittawattana/Documents/SeniorP/Soft_en/export_dataframe.csv', index = None, header=True)

# Train model
def model(result_):
    X = result_[['Conditions', 'temstar', 'temstop']]
    y = result_['Generated']
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)
    regressor = LinearRegression()
    regressor.fit(X_train, y_train)
    coeff_df = pd.DataFrame(regressor.coef_, X.columns, columns=['Coefficient'])
    # print(coeff_df)
    y_pred = regressor.predict(X_test)
    database = pd.DataFrame({'Actual': y_test, 'Predicted': y_pred})
    # print('Mean Absolute Error:', metrics.mean_absolute_error(y_test, y_pred))
    # print('Mean Squared Error:', metrics.mean_squared_error(y_test, y_pred))
    # print('Root Mean Squared Error:', np.sqrt(metrics.mean_squared_error(y_test, y_pred)))
    return y_test,y_pred
# Condition -> Showers is 0
#              Fine is 1
#              Partly Cloudy is 2
#              Mostly Cloudy is 3
#              Cloudy is 4
con0 = result.loc[result['Conditions'] == 0]
con1 = result.loc[result['Conditions'] == 1]
con2 = result.loc[result['Conditions'] == 2]
con3 = result.loc[result['Conditions'] == 3]
con4 = result.loc[result['Conditions'] == 4]

y_test0,y_pred0 = model(con0)
y_test1,y_pred1 = model(con1)
y_test2,y_pred2 = model(con2)
y_test3,y_pred3 = model(con3)
y_test4,y_pred4 = model(con4)
print(con0)

generator0 = con0['Generated'].sum() / len(con0)
generator1 = con1['Generated'].sum() / len(con1)
generator2 = con2['Generated'].sum() / len(con2)
generator3 = con3['Generated'].sum() / len(con3)
generator4 = con4['Generated'].sum() / len(con4)







